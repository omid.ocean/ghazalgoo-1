import {Schema,model} from "mongoose";
import mongoosePaginate from "mongoose-paginate";

const schema =  new Schema({
    name: {
        type:String,
        required:true,
        lowercase:true,
        trim:true,
        minlength:3
    },
    description: {
        type:String,
        required: false,
        lowercase:true,
        trim:true,
        minlength:30
    },
    image: {
        type:String,
        required: false,
        lowercase:true,
        trim:true,
        minlength:10
    },
    likes: {
        type:Number,
        min:0
    },
    categories:[{ type: Schema.Types.ObjectId, ref: 'Category'}]
});

schema.index({name : 'text'});

schema.plugin(mongoosePaginate);

export const Poet = model('Poet',schema);