import {Router} from "express";
import {User} from "../models/user";
import {ErorResponse, InternalErrorCodes} from "../tools/responses";
import i18n from "i18n";
// import {logger} from "../startup/logging";
import bcrypt from 'bcrypt';
import Joi from 'joi';

const router = Router();

router.post('/', async (req,res) => {
    i18n.setLocale(req.locale);
    try {
        const body = await validate(req);

        // @ts-ignore
        let user = await User.findOne({email : body.email});
        if (!user) {
            const err = new ErorResponse(InternalErrorCodes.InvalidEmail);
            return res.status(err.responseHeader()).send(err.response())
        }
        // @ts-ignore
        if (await bcrypt.compare(body.password,user.passwordHash)){
            const err = new ErorResponse(InternalErrorCodes.InvalidEmail);
            return res.status(err.responseHeader()).send(err.response())
        }

        //todo valid login send JWT token
        res.send(true);
    }catch (e) {

    }
});

const validate = (req:object)=> {
    const schema = Joi.object().keys({
        email: Joi.string().email().required().error(new Error('Please enter a valid email!')),
        password: Joi.string().required().regex(/^[a-zA-Z0-9]{3,30}$/).error(new Error('Your password must be alphanumeric and between 3-30 characters!'))
    });
    return Joi.validate(req,schema,{
        stripUnknown:true
    });
};

export default router;