import {Router} from "express";
import {User, validate} from "../models/user";
import {Types} from "mongoose";
import {pick} from 'lodash';
import {logger} from "../startup/logging";
import bcrypt from 'bcrypt';
import {ErorResponse, InternalErrorCodes} from "../tools/responses";
import i18n from "i18n";

const router = Router();

router.post('/', async (req,res) => {
    try {
        i18n.setLocale(req.locale);
        let validation = await validate(req.body);
        // @ts-ignore
        validation.passwordHash = await bcrypt.hash(validation.password, await bcrypt.genSalt(8));
        // @ts-ignore
        validation.access = 'user';
        // @ts-ignore
        validation.telegram = {
            _id: Types.ObjectId()
        };

        console.log(JSON.stringify(validation,undefined,2));
        const user = new User(validation);
        const resault = pick(await user.save(),['_id','name','lName','email']);
        res.send(resault);
    }catch (e) {
        if (e.code && e.code === 11000) {
            e = new ErorResponse(InternalErrorCodes.EmailAlreadyExists);
        }else if (e.message) {
            e = new ErorResponse(InternalErrorCodes.Validator,e.message);
        } else {
            logger.error(e);
            e = new ErorResponse(InternalErrorCodes.UnHandled);
        }
        res.status(e.responseHeader()).send(e.response());
    }
});
export default router;