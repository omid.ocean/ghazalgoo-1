import {Router} from "express";
import {Category} from "../models/category";
import {logger} from "../startup/logging";
import {toNumber,validateId} from "../tools/Tools";
import {ErorResponse, InternalErrorCodes} from "../tools/responses";
import i18n from 'i18n';

const router = Router();

router.get('/', async (req,res)=> {
    if (!req.query.page)  req.query.page = 1;
    if (!req.query.limit) req.query.limit = 10;
    i18n.setLocale(req.locale);
    if (req.query.search && req.query.search.length < 3) {
        const err = new ErorResponse(InternalErrorCodes.ShortQuery);
        return res.status(err.responseHeader()).send(err.response())
    }
    const search = req.query.search? { $text : { $search : req.query.search } } : {isParent:true};
    try {
    const categories = await Category.paginate(search,{page : toNumber(1,req.query.page), limit: toNumber(5,req.query.limit), select: 'title description'});
    if (categories.page && categories.pages && (categories.page > categories.pages || categories.page < 1))  {
        const err = new ErorResponse(InternalErrorCodes.PageOutOfBounds);
        return res.status(err.responseHeader()).send(err.response())
    }
        res.send({
            categories : categories.docs,
            total : categories.total,
            limit : categories.limit,
            page : categories.page,
            pages : categories.pages
        });
    }catch (e) {
        logger.error(e);
    }
});

router.get('/:id',async (req,res) => {
    i18n.setLocale(req.locale);
    if (!validateId(req.params.id)) {
        const err = new ErorResponse(InternalErrorCodes.InvalidObjectId);
        return res.status(err.responseHeader()).send(err.response())
    }
    //todo add pagination to poems and categories
    const category = await Category
        .findById(req.params.id)
        .populate('subCategories','title description parent')
        .populate('poems','poemName')
        .populate('poet','name')
        .populate('parent', 'title');
    if (category) res.send({category});
    else {
        const err = new ErorResponse(InternalErrorCodes.NotFound);
        return res.status(err.responseHeader()).send(err.response())
    }
});

//todo add post route afer adding auth!

export default router;