import {Router} from "express";
import {Poet} from "../models/poet";
import {logger} from "../startup/logging";
import {toNumber,validateId} from "../tools/Tools";
import {ErorResponse, InternalErrorCodes} from "../tools/responses";
import i18n from 'i18n';

const router = Router();

router.get('/', async (req,res)=> {
    if (!req.query.page)  req.query.page = 1;
    if (!req.query.limit) req.query.limit = 10;
    i18n.setLocale(req.locale);
    if (req.query.search && req.query.search.length < 3) {
        const err = new ErorResponse(InternalErrorCodes.ShortQuery);
        return res.status(err.responseHeader()).send(err.response())
    }
    const search = req.query.search? { $text : { $search : req.query.search } } : {};
    try {
        const poets = await Poet.paginate(search,{page : toNumber(1,req.query.page), limit: toNumber(5,req.query.limit), select: 'name description image'});
        if (poets.page && poets.pages && (poets.page > poets.pages || poets.page < 1))  {
            const err = new ErorResponse(InternalErrorCodes.PageOutOfBounds);
            return res.status(err.responseHeader()).send(err.response())
        }
        res.send({
            poets : poets.docs,
            total : poets.total,
            limit : poets.limit,
            page : poets.page,
            pages : poets.pages
        });
    }catch (e) {
        logger.error(e);
    }
});

router.get('/:id',async (req,res) => {
    i18n.setLocale(req.locale);
    if (!validateId(req.params.id)) {
        const err = new ErorResponse(InternalErrorCodes.InvalidObjectId);
        return res.status(err.responseHeader()).send(err.response())
    }
    //todo add pagination to poems and categories
    const poet = await Poet
        .findById(req.params.id)
        .populate('categories','title description');
    if (poet) res.send({poet});
    else {
        const err = new ErorResponse(InternalErrorCodes.NotFound);
        return res.status(err.responseHeader()).send(err.response())
    }
});

//todo add post route afer adding auth!

export default router;