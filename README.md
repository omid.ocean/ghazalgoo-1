# Ghazalgoo
Ghazalgoo is a passion project of mine to have a modern service to read my favourite poems anywhere. Current systems such as ganjoor.com already provide the biggest library of poems but sadly it's old and not much functionality is implemented. Since they offered their database for public use, I decided to create this service and achieve my goal.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What  you need to install the software and how to install them

```
Nodejs v 8+
TypeScript v 3+
MongoDB v 4+
```

## Built With

* [Node.js](http://nodejs.org/) - JavaScript framework
* [MongoDB](https://www.mongodb.com/) - Database
* [Typescript](https://www.typescriptlang.org/) - development language

## API Documentations

you can read the API documentations **[HERE](https://documenter.getpostman.com/view/5156077/RWgjb3LJ) ** .
please note that this documentation is not the goal and only represents the current state of development and is subject to change with each build.


## Authors

* **Omid Astaraki** - *Initial work* - [Electather](https://github.com/Electather)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* This project was made possible by Ganjoor.com 
