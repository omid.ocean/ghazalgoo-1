import express from 'express';
import {logger} from "./startup/logging";
import config from './startup/config'
import {initDatabase} from './startup/mongoose';
import routes from "./startup/routes";

const app = express();

config();
initDatabase();
routes(app);

//Start listening on provided port by system
const port = process.env.PORT;
app.listen(port,()=> logger.info(`listening on port ${port}...`));
// exporting app for other modules
export {app}
