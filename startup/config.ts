import config from 'config';
import i18n from 'i18n';
import {logger} from "./logging";

//detecting Environment
const env = process.env.NODE_ENV || 'development';

//setting variables
export default () => {
    if (env === 'development'){
        process.env.PORT = "3001";
        process.env.MONGODB_URI= 'mongodb://localhost:27017/ganjoor'
    } else if (env === 'test'){
        process.env.PORT = "3001";
        process.env.MONGODB_URI= 'mongodb://localhost:27017/ganjoorTest'
    }

    //check if config files exist by reading JWT value
    if (!config.get('jwtPrivateKey')) {
        // throw new Error('FATAL ERROR: jwtPrivateKey is not defined.');
    }

    // configuring i18n
    i18n.configure({
        locales:['en', 'fa'],
        directory: './locales',
        defaultLocale: 'en',
        logDebugFn: function (msg) {
            logger.debug(msg);
        },

        // setting of log level WARN - default to require('debug')('i18n:warn')
        logWarnFn: function (msg) {
            logger.warn(msg);
        },

        // setting of log level ERROR - default to require('debug')('i18n:error')
        logErrorFn: function (msg) {
            logger.error(msg);
        },
    });

};
