import bodyParser from 'body-parser';
import {Express} from "express";
import categories from '../routes/categories'
import poets from '../routes/poets';
import poems from '../routes/poems';
import users from '../routes/users';
import auth from '../routes/auth';
import {init} from 'i18n';

//Setting up Middleware
export default (app : Express)=> {
    app.use(bodyParser.json());
    app.use(init);
    app.use('/api/categories', categories);
    app.use('/api/poets',poets);
    app.use('/api/poems', poems);
    app.use('/api/users', users);
    app.use('/api/auth',auth);
};