import {ObjectID} from "bson";

export const toEngNumerals = (input:any) =>{
    return input.replace(/[\u0660-\u0669\u06f0-\u06f9]/g, (c: any) => {
        return (c.charCodeAt(0) & 0xf);
    });
};

export const toPersianNumerals = (input:any) => {
    return input.replace(/[0-9]/g, (c:any) => {
            return c.charCodeAt(0) + 1776;
    });
};

export const toNumber = (minimum:number, arg: number | string, max?:number) : number => {
    if (typeof arg !== 'number') arg = parseFloat(arg);
    max = max ? max : 100;
    if (arg > minimum && arg<max) return arg ;
    else if (arg > minimum) return max ;
    else return minimum;
};

export function validateId(id: string) : boolean {
    if (id) return ObjectID.isValid(id);
    else return false;
}
